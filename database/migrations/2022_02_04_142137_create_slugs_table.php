<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlugsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('slug_collections', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->unique();
            $table->timestamps();

        });

        Schema::create('slugs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('collection_id')->nullable()->constrained('slug_collections')->onDelete('cascade');
            $table->string('slug');
            $table->string('locale')->default('');
            $table->numericMorphs('sluggable');
            $table->timestamps();
            $table->unique(['slug','collection_id','locale']);
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('slugs');
        Schema::dropIfExists('slug_collections');
    }
}
