<?php

namespace KDA\Sluggable\Database\Factories;

use KDA\Sluggable\Models\Slug;
use Illuminate\Database\Eloquent\Factories\Factory;

class SlugFactory extends Factory
{
    protected $model = Slug::class;

    public function definition()
    {
        return [
         
        ];
    }
}
