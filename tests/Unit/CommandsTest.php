<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Support\Facades\Artisan;
use KDA\Tests\TestCase;
use DB;
use KDA\Tests\Models\Post;

class CommandsTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function commands_exists()
  {

    \DB::insert('INSERT into posts (title) VALUES (?)', ["Unslugged Title Test"]);


    $p = Post::first();
    $this->assertEquals("Unslugged Title Test", $p->title);
    $this->assertEquals(0, $p->slugs->count());



    Artisan::call('kda:slugs:generate',['model'=>'\KDA\Tests\Models\Post']);
    $p = Post::first();
    $this->assertEquals(1, $p->slugs->count());

  }

   /** @test */
   function commands_ran_twice()
   {
 
     \DB::insert('INSERT into posts (title) VALUES (?)', ["Unslugged Title Test"]);
 
 
     
     Artisan::call('kda:slugs:generate',['model'=>'\KDA\Tests\Models\Post']);
     $p = Post::first();
     $this->assertEquals(1, $p->slugs->count());
 
     Artisan::call('kda:slugs:generate',['model'=>'\KDA\Tests\Models\Post']);
     $p = Post::first();
     $this->assertEquals(1, $p->slugs->count());
   }



  /** @test */
/* function a_slug_has_a_collection()
  {
    $c = SlugCollection::factory()->create(['name'=>'Pages']);
    $o = Slug::factory()->create(['slug' => 'Fake Title','collection_id'=>$c->id]);
    $this->assertEquals('Fake Title', $o->slug);
    $this->assertEquals($c->id, $o->collection->id);

  }
*/


  
}