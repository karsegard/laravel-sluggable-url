<?php

namespace KDA\Sluggable\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use KDA\Sluggable\Facades\Slug ;

class Regenerate extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:slugs:regenerate {model} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    public function __construct(Filesystem $files)
    {
        parent::__construct();

    }


    public function fire()
    {
        return $this->handle();
    }


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $model = $this->argument('model');
        
        \KDA\Sluggable\Models\Slug::where('sluggable_type',$model)->delete();
        $r = $model::withNoSlug()->get();
        foreach($r as $item){
            Slug::createSlugForModel($item);
        }
    }
}
