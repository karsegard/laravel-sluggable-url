<?php

namespace KDA\Sluggable\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use KDA\Sluggable\Facades\Slug ;
use KDA\Sluggable\Models\Slug as ModelsSlug;

class Fix extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:slugs:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    public function __construct(Filesystem $files)
    {
        parent::__construct();

    }


    public function fire()
    {
        return $this->handle();
    }


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $classes =ModelsSlug::select('sluggable_type')->distinct()->get();
        foreach($classes as $class){
            $c = $class->sluggable_type;
            try {
                $model =    resolve($c);
            }catch (\Exception $e){
                ModelsSlug::where('sluggable_type',$c)->delete();
            }
        }
    }
}
