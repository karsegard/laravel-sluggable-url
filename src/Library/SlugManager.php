<?php

namespace KDA\Sluggable\Library;

use Closure;
use Str;
use KDA\Sluggable\Models\SlugCollection;
use Illuminate\Database\Eloquent\Model;
use KDA\Sluggable\Models\Slug;
use KDA\Sluggable\Models\Contracts\RegisterSlugs;

class SlugManager
{
    public function slugify($string)
    {
        return Str::slug($string, '_');
    }

    public function collection($name)
    {
        return SlugCollection::forSlug($name)
            ->with('slugs')
            ->first();
    }

    public function getFirstByPath($path, $separator = '/')
    {
        [$collection, $slug] = explode($separator, $path);
        return $this->getFirstByCollection($collection, $slug);
    }

    public function getFirstByCollection($collection, $slug)
    {
        $s = Slug::where('slug', $slug)
            ->whereHas('collection', function ($q) use ($collection) {
                $q->where('slug', $collection);
            })
            ->first();

        if ($s) {
            return $s->sluggable;
        }

        return null;
    }

    public function ensureModelIsOk($model)
    {
        if (!($model instanceof RegisterSlugs)) {
            throw new \Exception('Model ' . get_class($model) . " doesn't implement " . RegisterSlugs::class);
        }
    }

    public function createSlugForModel(Model $model, $collection = 'default', $locale = '')
    {
     //   $this->ensureModelIsOk($model);
        if (method_exists($model, 'slugCollectionName')) {
            $collection = $model->slugCollectionName();
        }
        $generate = true;
        if (method_exists($model, 'shouldGenerateSlug')) {
            $generate = $model->shouldGenerateSlug();
        }
        if (method_exists($model, 'getSluggableLocale')) {
            $locale = $model->getSluggableLocale();
        }
        
        if ($generate) {
            $sluggable = $model->sluggable;

            if ($sluggable) {
                $sluggable = $this->slugify($sluggable);

                $c = $this->retrieveCollection($collection);

                $existing = Slug::where('slug', $sluggable)
                    ->where('collection_id', $c->id)
                    ->where('locale', $locale)
                    ->count();

                if (!config('kda.sluggable.forbid_identical_slug', true) && config('kda.sluggable.identical_slug_strategy', 'increment') == 'increment') {
                    if ($existing > 0) {
                        $sluggable .= '_' . $model->id;
                    }
                }

                Slug::create([
                    'sluggable_id' => $model->id,
                    'sluggable_type' => get_class($model),
                    'collection_id' => $c->id,
                    'locale'=>$locale,
                    'slug' => $sluggable,
                ]);
            }
        }
    }

    public function getCollectionNameForModel($model): string
    {
        $collection = config('kda.sluggable.default_collection', 'default');
        if (method_exists($model, 'slugCollectionName')) {
            $collection = $model->slugCollectionName();
        }
        return $collection;
    }

    public function evaluate($value, array $parameters = [])
    {
        if ($value instanceof Closure) {
            return app()->call($value, $parameters);
        }

        return $value;
    }

    public function retrieveCollection(SlugCollection|int|string |Closure |Model $collection, $create = true):SlugCollection
    {
        //if this is a model retrieve the collection from model first 

        if ($collection instanceof RegisterSlugs){
            $collection = $this->getCollectionNameForModel($collection);
        }

        $collection = $this->evaluate($collection);

        if (is_string($collection)) {
            $c = SlugCollection::forSlug($collection)->first();
            if (!$c && $create === true) {
                $c = new SlugCollection();
                $c->name = $collection;
                $c->save();
            }
            return $c;
        } else if (is_int($collection)) {
            return SlugCollection::find($collection);
        } else if ($collection instanceof SlugCollection) {
            return $collection;
        } 
        return null;
    }

    public function getExistingSlugCount(string $slug, SlugCollection|int|string|Closure | Model $collection, $locale = ''): int
    {
        $collection = $this->retrieveCollection($collection);
        return Slug::where('slug', $slug)
            ->where('collection_id', $collection->id)
            ->where('locale', $locale)
            ->count();
    }

    public function createSlug($model, $slug, $deduplicate = false, $locale = '')
    {
        $c = $this->retrieveCollection($model);


        if ($deduplicate) {
            if (!config('kda.sluggable.forbid_identical_slug', true) && config('kda.sluggable.identical_slug_strategy', 'increment') == 'increment') {
                if ($existing > 0) {
                    $slug .= '_' . $model->id;
                }
            }
        }

        return Slug::create([
            'sluggable_id' => $model->id,
            'sluggable_type' => get_class($model),
            'collection_id' => $c->id,
            'slug' => $slug,
        ]);
    }

    public function updateSlug($model, $slug, $locale = ''){
        $c = $this->retrieveCollection($model);

        if($this->getExistingSlugCount($slug,$c,$locale)>0){
            throw new \Exception("Slug already exists. Please delete it first");
        }
        $model->currentSlug->slug=$slug;
        $model->currentSlug->save();
    }

    public function updateSlugForModel(Model $model, $collection = 'default', $locale = '')
    {
        if (method_exists($model, 'slugCollectionName')) {
            $collection = $model->slugCollectionName();
        }
        $generate = true;
        if (method_exists($model, 'shouldGenerateSlug')) {
            $generate = $model->shouldGenerateSlug();
        }
        if (method_exists($model, 'getSluggableLocale')) {
            $locale = $model->getSluggableLocale();
        }
        if ($generate) {
            $sluggable = $model->sluggable;
            if ($sluggable) {
                $sluggable = $this->slugify($sluggable);

                $existing =
                    Slug::whereHasMorph('sluggable', get_class($model), function ($q) use ($model) {
                        $q->where('sluggable_id', $model->id);
                    })
                    ->get()
                    ?->pluck('slug')
                    ->toArray() ?? [];
              //  dump($sluggable,$existing);
                if (blank($existing)) {
                  //  dump($existing);
                  //  dump($sluggable);
                    $this->createSlugForModel($model, $collection, $locale);
                }
            }
        }
    }

    public function deleteSlugForModel(Model $model, $collection = 'default', $locale = '')
    {
        if (method_exists($model, 'slugCollectionName')) {
            $collection = $model->slugCollectionName();
        }

        Slug::whereHasMorph('sluggable', get_class($model), function ($q) use ($model) {
            $q->where('sluggable_id', $model->id);
        })->delete();
    }
}
