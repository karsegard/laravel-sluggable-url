<?php

namespace KDA\Sluggable\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use Str;
class SlugCollection extends Model 
{
    use HasFactory;

    protected $fillable = [
       'name',
       'slug',
    ];
 
    public function slugs()
    {
        return $this->hasMany(Slug::class,'collection_id','id');
    }

    public function setSlugAttribute($value){
        if(!$this->name){
            $this->name = $value;
        }

        $this->attributes['slug']= $value;
    }

    public function setNameAttribute($value){
        if(!$this->slug){
            $this->slug = Str::slug($value,'_');
        }

        $this->attributes['name']= $value;
    }

    

    protected static function newFactory()
    {
        return  \KDA\Sluggable\Database\Factories\SlugCollectionFactory::new();
    }

    public function scopeForSlug($query,$slug){
        return $query->where('slug',$slug);
    }
}
