<?php

namespace KDA\Sluggable\Models\Traits;

use KDA\Sluggable\Facades\Slug as SlugFacade;
use KDA\Sluggable\Models\Contracts\RegisterSlugsRoute;
use KDA\Sluggable\Models\Slug as Slug;

trait HasSlugs
{


    public static function bootHasSlugs()
    {
        static::created(function ($item) {
            SlugFacade::createSlugForModel($item);
        });

        static::updated(function ($item) {
            SlugFacade::updateSlugForModel($item);
        });

        static::deleting(function ($item) {
            SlugFacade::deleteSlugForModel($item);
        });


    }

    public function slugs()
    {
        return $this->morphMany(Slug::class, 'sluggable');
    }

    public function currentSlug()
    {
        return $this->morphOne(Slug::class, 'sluggable')->latestOfMany();
    }

    public function scopeWithNoSlug($query)
    {
        return $query->whereDoesntHave('slugs');
    }

    public function scopeForSlug($query,$slug)
    {
        return $query->whereHas('slugs',function($q) use($slug){
            $q->where('slug',$slug);
        });
    }

    public function getUrlAttribute()
    {
        $route = false;
        if( $this instanceof RegisterSlugsRoute){
            $route = true;
        }

        $slug = $this->currentSlug;

        if($route&& $slug){
            return route($this->getSluggableRouteName(),['slug'=>$slug->slug]);
        }
        if ($slug) {
            return '/' . $slug->getFullPath();
        }
        return '';
    }
    public function getSlugAttribute(){
        if($this->currentSlug){
           return $this->currentSlug->slug;
        }
        return NULL;
    }
}
