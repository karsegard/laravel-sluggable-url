<?php

namespace KDA\Sluggable\Models\Contracts;

interface RegisterSlugs
{

    public function slugCollectionName();

    public function getSluggableAttribute();

  //  public function shouldGenerateSlug():boolean;
}
