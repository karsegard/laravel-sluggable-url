<?php

namespace KDA\Sluggable\Models\Contracts;

interface RegisterSlugsRoute
{

    public function getSluggableRouteName();

  //  public function shouldGenerateSlug():boolean;
}
