<?php

namespace KDA\Sluggable\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Sluggable\Facades\Slug as SlugFacade;


class Slug extends Model
{
    use HasFactory;

    protected $fillable = [
        'slug',
        'collection_id',
        'locale',
        'sluggable_id',
        'sluggable_type'

    ];

    protected $appends = [
        'full_path'
    ];

    protected $casts = [
        'is_current'=> 'boolean'
    ];


    public function sluggable()
    {
        return $this->morphTo();
    }

    public function collection()
    {
        return $this->belongsTo(SlugCollection::class, 'collection_id', 'id');
    }
    protected static function newFactory()
    {
        return  \KDA\Sluggable\Database\Factories\SlugFactory::new();
    }

    public function getFullPathAttribute()
    {
        return $this->collection->slug . '/' . $this->slug;
    }

    public function getIsCurrentAttribute()
    {
        if (!$this->sluggable) {
            return false;
        }
        return $this->id == $this->sluggable->currentSlug->id;
    }

    public function getFullPath($separator = '/')
    {

        $components = [];
        //$this->load('collection');
        if ($this->collection) {
            $components[] = $this->collection->slug;
        }

        $components[] = $this->slug;

        return collect($components)->join($separator);
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = SlugFacade::slugify($value);
    }
    public function getUrlAttribute()
    {
            return '/' . $this->full_path;
    }
}
