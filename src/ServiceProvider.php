<?php

namespace KDA\Sluggable;

use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasLoadableMigration;
    use \KDA\Laravel\Traits\HasDumps;
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasConfig;
    protected $configs = [
        'kda/sluggable.php'  => 'kda.sluggable'
    ];
    protected $dumps = [
        'slugs',
        'slug_collections'
    ];
    protected $_commands = [
        Commands\GenerateSlugs::class,
        Commands\Regenerate::class,
        Commands\Fix::class
    ];

    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

    public function register()
    {
        $this->app->singleton('kda.slugmanager', function ($app) {
            return new Library\SlugManager();
        });
    }
}
